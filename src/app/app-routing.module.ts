import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './home/home.component';

import { PositionListComponent } from './positions/position-list/position-list.component';
import { DepartmentListComponent } from './departments/department-list/department-list.component';
import { EmployeeListComponent } from './employees/employee-list/employee-list.component';
 
const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent},
  { path: 'positions',   component: PositionListComponent },
  { path: 'departments', component: DepartmentListComponent },
  { path: 'employees',   component: EmployeeListComponent }
];
 
@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}