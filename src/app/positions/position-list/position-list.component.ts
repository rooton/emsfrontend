import { Component, OnInit } from '@angular/core';

import { Position } from '../shared/position';
import { PositionService } from '../shared/position.service';

@Component({
    selector: 'position-list',
    templateUrl: './position-list.component.html',
    styleUrls: [ './position-list.component.css' ]
}) 
export class PositionListComponent implements OnInit{

    positions: Position[];
    selectedPosition: Position;

    constructor (
    
        private positionService: PositionService
    
    ) {}

    ngOnInit() : void {
        this.positionService.getPositions()
        .then(positions => this.positions = positions);
    }

    onSelect(position: Position) {
        this.selectedPosition = position;
    }

    add(name: string): void {
        name = name.trim();
        if (!name) { return; }
        this.positionService.create(name)
        .then(position => {
            this.positions.push(position);
            this.selectedPosition = null;
        });
    }

    save(position: Position): void {
      this.positionService.update(position).then(position => {
            this.selectedPosition = null;
        });
    }

    delete(position: Position): void {
        this.positionService.delete(position.id)
            .then(() => {
            this.positions = this.positions.filter(h => h !== position);
            if (this.selectedPosition === position) { this.selectedPosition = null; }
            });
    }
}