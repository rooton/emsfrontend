import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Position } from './position';

@Injectable()
export class PositionService {

    private url = '/api/position';
    private headers = new Headers({'Content-Type': 'application/json'});

    constructor(
       
        private http: Http
    
    ) {}

    getPositions(): Promise<Position[]> {

        return this.http.get(this.url)
             .toPromise()
             .then(response => response.json()._embedded.position as Position[])
             .catch(this.handleError);
    }

    create(name: string): Promise<Position> {
        
        let body  = JSON.stringify({name: name});

        return this.http
            .post(this.url, body, {headers: this.headers})
            .toPromise()
            .then(res => res.json() as Position)
            .catch(this.handleError);
    }

    update(position: Position): Promise<Position> {
    
        const url = `${this.url}/${position.id}`;

        let body  = JSON.stringify(position);     

        return this.http
            .put(url, body, {headers: this.headers})
            .toPromise()
            .then(() => position)
            .catch(this.handleError);
    }

    delete(id: number): Promise<void> {
        
        const url = `${this.url}/${id}`;

        return this.http.delete(url, {headers: this.headers})
            .toPromise()
            .then(() => null)
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}