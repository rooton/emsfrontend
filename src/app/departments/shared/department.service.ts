import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Department } from './department';

@Injectable()
export class DepartmentService {

    private url = '/api/department';
    private headers = new Headers({'Content-Type': 'application/json'});

    constructor(
       
        private http: Http
    
    ) {}

    getDepartments(): Promise<Department[]> {

        return this.http.get(this.url)
             .toPromise()
             .then(response => response.json()._embedded.department as Department[])
             .catch(this.handleError);
    }

    create(body: string): Promise<Department> {

        return this.http
            .post(this.url, body, {headers: this.headers})
            .toPromise()
            .then(res => res.json() as Department)
            .catch(this.handleError);
    }

    update(id: number, body: string): Promise<Department> {
    
        const url = `${this.url}/${id}`;
    
        return this.http
            .put(url, body, {headers: this.headers})
            .toPromise()
            .then( res => res.json() as Department)
            .catch(this.handleError);
    }

    delete(id: number): Promise<void> {
        
        const url = `${this.url}/${id}`;

        return this.http.delete(url, {headers: this.headers})
            .toPromise()
            .then(() => null)
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}