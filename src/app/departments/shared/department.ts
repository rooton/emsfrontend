export class Department {
    id: number;
    name: string;
    parentId: number;
}