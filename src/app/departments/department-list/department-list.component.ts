import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { Department } from '../shared/department';
import { DepartmentService } from '../shared/department.service';

@Component({
    selector: '',
    templateUrl: './department-list.component.html',
    styleUrls: [ './department-list.component.css']
})
export class DepartmentListComponent implements OnInit {
    
    departments: Department[];
    selectedDepartment: Department;

    constructor (
    
        private departmentService: DepartmentService
    
    ) {}

    ngOnInit() : void {
        this.departmentService.getDepartments()
        .then(departments => this.departments = departments);
    }

    onSelect(department: Department) {
        this.selectedDepartment = department;
    }

    add(form: NgForm) {

        if ( form.value.parentId === undefined || form.value.parentId === "") {
            form.value.parentId = 0;
        }

        form.value.name = form.value.name.trim();
        
        let body = JSON.stringify(form.value);
        console.log(body);

        this.departmentService.create(body)
        .then(department => {
            this.departments.push(department);
            this.selectedDepartment = null;
            form.reset();
        });
    }

    update(form: NgForm): void {

        if ( form.value.parentId === undefined || form.value.parentId === "") {
            form.value.parentId = 0;
        }

        form.value.name = form.value.name.trim();
        
        let id = form.value.id;
        let body = JSON.stringify(form.value);
        console.log(body);
        
        this.selectedDepartment.parentId = form.value.parentId;

        this.departmentService.update(id, body)
        .then(department => {        
            this.selectedDepartment = null;
        });
    }

    delete(department: Department): void {
        this.departmentService.delete(department.id)
            .then(() => {
            this.departments = this.departments.filter(h => h !== department);
            if (this.selectedDepartment === department) { this.selectedDepartment = null; }
            });
    }
}