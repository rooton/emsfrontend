import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpModule }    from '@angular/http';

import { AppRoutingModule }     from './app-routing.module';

import { AppComponent } from './app.component';

import { HomeComponent } from './home/home.component';

import { PositionListComponent } from './positions/position-list/position-list.component';
import { PositionService } from './positions/shared/position.service';

import { DepartmentListComponent } from './departments/department-list/department-list.component';
import { DepartmentService } from './departments/shared/department.service';

import { EmployeeListComponent } from './employees/employee-list/employee-list.component';
import { EmployeeService } from './employees/shared/employee.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PositionListComponent,
    DepartmentListComponent,
    EmployeeListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [PositionService, DepartmentService, EmployeeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
