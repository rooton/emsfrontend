export class Employee {
    id: number;
    name: string;
    surname: string;
    email: string;
    phone: string;
    positionId: number;
    departmentId: number;
    parentEmployeeId: number;
    birthDate: string;
    employmentDate: string;
}