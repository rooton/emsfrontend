import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Employee } from './employee';

@Injectable()
export class EmployeeService {

    private url = '/api/employee';
    private headers = new Headers({'Content-Type': 'application/json'});

    constructor(
       
        private http: Http
    
    ) {}

    getEmployees(): Promise<Employee[]> {

        return this.http.get(this.url)
             .toPromise()
             .then(response => response.json()._embedded.employee as Employee[])
             .catch(this.handleError);
    }

    create(body: string): Promise<Employee> {

        return this.http
            .post(this.url, body, {headers: this.headers})
            .toPromise()
            .then(res => res.json() as Employee)
            .catch(this.handleError);
    }

    update(id: number, body: string): Promise<Employee> {
    
        const url = `${this.url}/${id}`;
    
        return this.http
            .patch(url, body, {headers: this.headers})
            .toPromise()
            .then( res => res.json() as Employee)
            .catch(this.handleError);
    }

    delete(id: number): Promise<void> {
        
        const url = `${this.url}/${id}`;

        return this.http.delete(url, {headers: this.headers})
            .toPromise()
            .then(() => null)
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}