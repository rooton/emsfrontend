import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { NgForm } from '@angular/forms';

import { Employee } from '../shared/employee';
import { EmployeeService } from '../shared/employee.service';

import { Department } from '../../departments/shared/department';
import { DepartmentService } from '../../departments/shared/department.service';

import { Position } from '../../positions/shared/position';
import { PositionService } from '../../positions/shared/position.service';

@Component({
    selector: '',
    templateUrl: './employee-list.component.html',
    styleUrls: [ './employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {
    
    employees: Employee[];
    departments: Department[];
    positions: Position[];
    selectedEmployee: Employee;
    datePipe = new DatePipe("en-US");

    constructor (
    
        private employeeService: EmployeeService,
        private departmentService: DepartmentService,
        private positionService: PositionService,
    
    ) {}

    ngOnInit() : void {
         this.employeeService.getEmployees()
        .then(employees => this.employees = employees);

         this.departmentService.getDepartments()
        .then(departments => this.departments = departments);

         this.positionService.getPositions()
        .then(positions => this.positions = positions);
    }

    onSelect(employee: Employee) {
        this.selectedEmployee = employee;
    }

    add(form: NgForm) {
    
        if ( form.value.positionId === undefined || form.value.positionId === "") {
            form.value.positionId = 0;
        }
        
        if ( form.value.parentEmployeeId === undefined || form.value.parentEmployeeId === "") {
            form.value.parentEmployeeId = 0;
        }       

        if ( form.value.departmentId === undefined || form.value.departmentId === "") {
            form.value.departmentId = 0;
        }
                
        form.value.birthDate = this.datePipe.transform(form.value.birthDate, 'dd.MM.yyyy');
        form.value.employmentDate = this.datePipe.transform(form.value.employmentDate, 'dd.MM.yyyy');

        let body = JSON.stringify(form.value);
        console.log(body);

        this.employeeService.create(body)
        .then(employee => {
            this.employees.push(employee);
            this.selectedEmployee = null;
            form.reset();
        });       
    }
    
    update(form: NgForm): void {

        if ( form.value.positionId === undefined || form.value.positionId === "") {
            form.value.positionId = 0;
        }
        
        if ( form.value.parentEmployeeId === undefined || form.value.parentEmployeeId === "") {
            form.value.parentEmployeeId = 0;
        }       

        if ( form.value.departmentId === undefined || form.value.departmentId === "") {
            form.value.departmentId = 0;
        }
       
        form.value.birthDate = this.datePipe.transform(form.value.birthDate, 'dd.MM.yyyy');
        form.value.employmentDate = this.datePipe.transform(form.value.employmentDate, 'dd.MM.yyyy');

        let id = form.value.id;
        let body = JSON.stringify(form.value);
        console.log(body);
        
        this.employeeService.update(id, body)
        .then(department => {        
            this.selectedEmployee = null;
        });
    }

    delete(employee: Employee): void {
        this.employeeService.delete(employee.id)
            .then(() => {
            this.employees = this.employees.filter(h => h !== employee);
            if (this.selectedEmployee === employee) { this.selectedEmployee = null; }
            });
    }
}