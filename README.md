# Ems

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.1.2.

## Development frontend server

NOTE!

Both frontend and backend in development mode.

You must run backend development server (https://bitbucket.org/rooton/emsbackend) on `http://localhost:8080/` before running this frontend development server.

1) Clone project, navigate to project folder and run `npm install`

2) Run `ng serve --proxy-conf proxy.conf.json` for a dev server. Navigate to `http://localhost:4200/`.

3) `proxy-conf proxy.conf.json` redirects all api requests to development backend server which on different server and port. CORS politics prohibits us to make request to different server and port.
